#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "libshrun.h"


int main(int argc, char **argv) {
	struct shrun_res res;
	// Check args
	if (argc == 1) {
		puts("Pls pass file");
		return EXIT_FAILURE;
	}
	// Run code
	res = shrun(argv[1], argc - 1, argv + 1);
	// Clean up
	shrun_clean(&res);
	// Exit
	return res.code;
}
