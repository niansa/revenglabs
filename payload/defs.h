#define NULL (void*)0

#define size_t unsigned long
#define ssize_t signed long
#define off_t size_t
#define time_t size_t
#define useconds_t size_t
#define seconds_t size_t
