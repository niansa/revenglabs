#include "defs.h"

size_t strlen(const char *s);
int print(const char *s, int fd);
int fputs(const char *s, int fd);
int puts(const char *s);
int usleep(useconds_t usec);
unsigned int sleep(unsigned int seconds);
int thread(size_t stack_size, void *func);
