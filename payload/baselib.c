#include "defs.h"
#include "moredefs.h"
#include "syscalls.h"


size_t strlen(const char *s) {
    size_t counter = 0;
    while(*(s++)) counter++;
    return counter;
}

int print(const char *s, int fd) {
    return write(fd, s, strlen(s))?0:-1;
}

int fputs(const char *s, int fd) {
    char newline = '\n';
    int res = print(s, fd);
    if (res != -1) {
        write(fd, &newline, 1);
    }
    return res;
}

int puts(const char *s) {
    return fputs(s, 1);
}

int usleep(useconds_t usec) {
    struct timespec time;
    time.tv_nsec = usec  * 1000;
    time.tv_sec = 0;
    return nanosleep(&time, NULL);
}

unsigned int sleep(unsigned int seconds) {
    struct timespec time;
    struct timespec time_rem;
    time.tv_nsec = 0;
    time.tv_sec = seconds;
    if (nanosleep(&time, &time_rem) == -1) {
        return time_rem.tv_sec;
    } else {
        return 0;
    }
}


int thread(size_t stack_size, void *func) {
    char *newstack = (char *)mmap(NULL, stack_size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK, -1, 0);
    if (newstack == MAP_FAILED) {
        return EXIT_FAILURE;
    }
    newstack += stack_size - sizeof(void **);
    *((void **)newstack) = func;
    // Create thread
    return clone(CLONE_FILES | CLONE_FS | CLONE_IO | CLONE_PTRACE | CLONE_VM, newstack, NULL, NULL, 0);
}
