#define EXIT_SUCCESS         0
#define EXIT_FAILURE         1

#define CLONE_VM             0x00000100      /* set if VM shared between processes */
#define CLONE_FS             0x00000200      /* set if fs info shared between processes */
#define CLONE_FILES          0x00000400      /* set if open files shared between processes */
#define CLONE_SIGHAND        0x00000800      /* set if signal handlers and blocked signals shared */
#define CLONE_PIDFD          0x00001000      /* set if a pidfd should be placed in parent */
#define CLONE_PTRACE         0x00002000      /* set if we want to let tracing continue on the child too */
#define CLONE_VFORK          0x00004000      /* set if the parent wants the child to wake it up on mm_release */
#define CLONE_PARENT         0x00008000      /* set if we want to have the same parent as the cloner */
#define CLONE_THREAD         0x00010000      /* Same thread group? */
#define CLONE_NEWNS          0x00020000      /* New mount namespace group */
#define CLONE_SYSVSEM        0x00040000      /* share system V SEM_UNDO semantics */
#define CLONE_SETTLS         0x00080000      /* create a new TLS for the child */
#define CLONE_PARENT_SETTID  0x00100000      /* set the TID in the parent */
#define CLONE_CHILD_CLEARTID 0x00200000      /* clear the TID in the child */
#define CLONE_DETACHED       0x00400000      /* Unused, ignored */
#define CLONE_UNTRACED       0x00800000      /* set if the tracing process can't force CLONE_PTRACE on this clone */
#define CLONE_CHILD_SETTID   0x01000000      /* set the TID in the child */
#define CLONE_NEWCGROUP      0x02000000      /* New cgroup namespace */
#define CLONE_NEWUTS         0x04000000      /* New utsname namespace */
#define CLONE_NEWIPC         0x08000000      /* New ipc namespace */
#define CLONE_NEWUSER        0x10000000      /* New user namespace */
#define CLONE_NEWPID         0x20000000      /* New pid namespace */
#define CLONE_NEWNET         0x40000000      /* New network namespace */
#define CLONE_IO             0x80000000      /* Clone io context */
#define CLONE_ARGS_SIZE_VER0 64              /* sizeof first published struct */

#define HUGETLB_FLAG_ENCODE_SHIFT     26
#define HUGETLB_FLAG_ENCODE_MASK      0x3f
#define HUGETLB_FLAG_ENCODE_64KB      (16 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_512KB     (19 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_1MB               (20 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_2MB               (21 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_8MB               (23 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_16MB      (24 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_32MB      (25 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_256MB     (28 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_512MB     (29 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_1GB               (30 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_2GB               (31 << HUGETLB_FLAG_ENCODE_SHIFT)
#define HUGETLB_FLAG_ENCODE_16GB      (34 << HUGETLB_FLAG_ENCODE_SHIFT)


#define MAP_GROWSDOWN        0x0100          /* stack-like segment */
#define MAP_DENYWRITE        0x0800          /* ETXTBSY */
#define MAP_EXECUTABLE       0x1000          /* mark it as an executable */
#define MAP_LOCKED           0x2000          /* pages are locked */
#define MAP_NORESERVE        0x4000          /* don't check for reservations */
#define MAP_TYPE             0x0f            /* Mask for type of mapping */
#define MAP_FIXED            0x10            /* Interpret addr exactly */
#define MAP_ANONYMOUS        0x20            /* don't use a file */
#define MAP_POPULATE         0x008000        /* populate (prefault) pagetables */
#define MAP_NONBLOCK         0x010000        /* do not block on IO */
#define MAP_STACK            0x020000        /* give out an address that is best suited for process/thread stacks */
#define MAP_HUGETLB          0x040000        /* create a huge page mapping */
#define MAP_SYNC             0x080000 /* perform synchronous page faults for the mapping */
#define MAP_FIXED_NOREPLACE  0x100000        /* MAP_FIXED which doesn't unmap underlying mapping */
#define MAP_UNINITIALIZED    0x4000000      /* For anonymous mmap, memory could be (...) */
#define MAP_FILE             0
#define MAP_SHARED           0x01            /* Share changes.  */
#define MAP_PRIVATE          0x02            /* Changes are private.  */
#define MAP_FIXED            0x10            /* Interpret addr exactly.  */
#define MAP_FAILED           ((void *) -1)
#define MAP_32BIT            0x40            /* only give out 32bit addresses */
#define MAP_SHARED           0x01            /* Share changes */
#define MAP_PRIVATE          0x02            /* Changes are private */
#define MAP_SHARED_VALIDATE  0x03      /* share + validate extension flags */
#define MAP_HUGE_SHIFT       HUGETLB_FLAG_ENCODE_SHIFT
#define MAP_HUGE_MASK        HUGETLB_FLAG_ENCODE_MASK
#define MAP_HUGE_64KB        HUGETLB_FLAG_ENCODE_64KB
#define MAP_HUGE_512KB       HUGETLB_FLAG_ENCODE_512KB
#define MAP_HUGE_1MB         HUGETLB_FLAG_ENCODE_1MB
#define MAP_HUGE_2MB         HUGETLB_FLAG_ENCODE_2MB
#define MAP_HUGE_8MB         HUGETLB_FLAG_ENCODE_8MB
#define MAP_HUGE_16MB        HUGETLB_FLAG_ENCODE_16MB
#define MAP_HUGE_32MB        HUGETLB_FLAG_ENCODE_32MB
#define MAP_HUGE_256MB       HUGETLB_FLAG_ENCODE_256MB
#define MAP_HUGE_512MB       HUGETLB_FLAG_ENCODE_512MB
#define MAP_HUGE_1GB         HUGETLB_FLAG_ENCODE_1GB
#define MAP_HUGE_2GB         HUGETLB_FLAG_ENCODE_2GB
#define MAP_HUGE_16GB        HUGETLB_FLAG_ENCODE_16GB

#define PROT_READ            0x1             /* page can be read */
#define PROT_WRITE           0x2             /* page can be written */
#define PROT_EXEC            0x4             /* page can be executed */
