#include "defs.h"

// Structs
struct timespec {
    time_t tv_sec;        /* seconds */
    long   tv_nsec;       /* nanoseconds */
};

// Syscalls
void exit(int status);
ssize_t write(int fd, const void *buf, size_t count);
int clone(size_t flags, void *stack, int *parent_tid, int *child_tid, size_t tls);
void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
int munmap(void *addr, size_t length);
int nanosleep(const struct timespec *req, struct timespec *rem);
