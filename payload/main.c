#include "baselib.h"
#include "moredefs.h"
#include "syscalls.h"



void payload_main() {
    const char test[] = "Hello world from child!";
    while(sleep(1) == 0) {
        puts(test);
    }

    // End
    exit(0);
}

int payload_start() {
    int res = thread(1024 * 1024 * 8, payload_main);
    return (res != -1) ? EXIT_SUCCESS : EXIT_FAILURE;
}


__attribute__((externally_visible, section(".init")))
int _start() {
    int res;
    const char loader_begin[] = "--- Loader begin ---";
    const char loader_end[] = "--- Loader end ---";
    const char text_startup[] = "Payload thread is starting... ";
    const char text_success[] = "Success\n";
    const char text_failure[] = "Failed\n";

    puts(loader_begin);
    print(text_startup, 1);

    // Start payload
    res = payload_start();

    // End
    print((res == EXIT_SUCCESS)?text_success:text_failure, 1);
    puts(loader_end);
    return res;
}
