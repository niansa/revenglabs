#include "syscalls.h"
#include "defs.h"
long (__syscall)(long, ...);


// Syscalls
void exit(int status) { // 60
    __syscall(60, status);
}
ssize_t write(int fd, const void *buf, size_t count) { // 1
    return __syscall(1, fd, buf, count);
}
int clone(size_t flags, void *stack, int *parent_tid, int *child_tid, size_t tls) { // 56
    return __syscall(56, flags, stack, parent_tid, child_tid, tls);
}
void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) { // 9
    return (void*)__syscall(9, addr, length, prot, flags, fd, offset);
}
int munmap(void *addr, size_t length) { // 11
    return __syscall(11, addr, length);
}
int nanosleep(const struct timespec *req, struct timespec *rem) { // 35
    return __syscall(35, req, rem);
}
