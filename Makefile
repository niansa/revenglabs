all: object binary


object:
	gcc --static -Wall -fno-builtin -fno-exceptions -fno-stack-protector -nodefaultlibs -nostartfiles ./payload/*.c ./payload/*.s -o payload.o

binary: object
	objcopy -O binary -j .init -j .text payload.o payload.bin

shrun:
	gcc ./shrun.c ./libshrun/libshrun.c -I./libshrun -o ./shrun


test: all shrun
	@echo
	./shrun ./payload.bin

clean:
	rm -f shrun *.o *.bin
