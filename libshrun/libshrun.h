struct shrun_res {
	int code;
	int fd;
	void *fmap;
};


struct shrun_res shrun(const char *inpath, int argc, char **argv);
void shrun_clean(struct shrun_res *res);
