#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "libshrun.h"

typedef int (*shfunc)(int argc, char **argv);
size_t inlen = 10000;


struct shrun_res shrun(const char *inpath, int argc, char **argv) {
	struct shrun_res res;
	// Prepare res with error data
	res.code = -1;
	res.fd = -1;
	res.fmap = NULL;
	// Open input file
	res.fd = open(inpath, O_RDONLY);
	if (!res.fd) {
		perror("Failed to open input file");
		return res;
	}
	// Create mmap from it
	res.fmap = mmap(NULL, inlen, PROT_READ | PROT_EXEC, MAP_PRIVATE, res.fd, 0);
	if (res.fmap == (void*)-1) {
		perror("Creating memory map failed");
		res.fmap = NULL;
		return res;
	}
	// Run sh code
	res.code = ((shfunc)res.fmap)(argc, argv);
	// End
	return res;
}

void shrun_clean(struct shrun_res *res) {
	munmap(res->fmap, inlen);
	close(res->fd);
}
