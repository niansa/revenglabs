Inject the generated injection.bin into any x86_64 executable and see the magic happening.

Please note that "make test" might segfault, but as long as the payload itself starts, it should be working.
